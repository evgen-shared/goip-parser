package summary

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/antchfx/xmlquery"
	"github.com/rs/zerolog"

	"gitlab.com/dummy-gopher/goip-parser/pkg/internal/logging"
)

var (
	log zerolog.Logger
)

func init() {
	log = logging.New().With().Str("package", "status").Str("subpackage", "summary").Logger()
}

type Status struct {
	Line                  int                // line number
	GSMModuleStatus       bool               // gsm module status. false -down, true - up
	SimCard               bool               // sim status. false - NoSim, true -detected
	GSMRegistration       bool               // gsm connection status. false - gsm is down, true - gsm is up
	VoIPRegistration      bool               // voip connection status. false - line not connected to voip, true - line connected to voip
	VoIPStatus            string             // current line state.
	SMSServerLogin        bool               // sms server connected
	ACD                   time.Duration      // average call duration
	ASR                   int                // average call success ratio
	TotalCallDuration     time.Duration      // total line calls duration
	ConnectedCallsCount   int                // successfull calls count
	TotalCallsCount       int                // total calls count
	CDRStartTime          time.Time          // CallDetailedRecoding start time
	RSSI                  int                // RSSI dbm
	Carrier               string             // Carrier name
	BCCH                  interface{}        // not recognized
	IdleTime              time.Duration      // idle time duration since last call
	CallRemainingCount    CallRemainCount    // remaing of calls count
	CallRemainingDuration CallRemainDuration // remaining of calls duration
	SMSRemaining          SMSRemain          // remaing of SMS count
}

type CallRemainCount struct {
	State       bool // calls remaining count is set
	CountRemain int  // calls remaing count
}

type CallRemainDuration struct {
	State      bool          // call remaining duration is set
	TimeRemain time.Duration // call remaining duration
}

type SMSRemain struct {
	State       bool // sms remaining count is set
	CountRemain int  // sms remaining count
}

// ReadSummary - parse xml body of status page
func ReadSummary(lines int, body string) ([]Status, error) {
	xmlResponse, err := xmlquery.Parse(strings.NewReader(body))
	if err != nil {
		log.Error().Err(err).Msg("xml parse failed")
		return nil, err
	}
	reGSM := regexp.MustCompile(">Y</a>")
	reSMSSrv := regexp.MustCompile(">N</font>")
	status := make([]Status, 0)
	for i := 1; i <= lines; i++ {
		line := Status{}
		lineNumber := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_module_status_gsm", i))
		num, err := strconv.Atoi(lineNumber.InnerText())
		if err != nil {
			log.Warn().Err(err).Msg("convert line number from string to int failed")
			num = 0
		}
		line.Line = num
		gsm := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_module_status", i))
		line.GSMModuleStatus = reGSM.Match([]byte(gsm.InnerText()))
		sim := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_gsm_sim", i))
		line.SimCard = strings.EqualFold(sim.InnerText(), "Y")
		gsmRegistration := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_gsm_status", i))
		line.GSMRegistration = strings.EqualFold(gsmRegistration.InnerText(), "Y")
		voipRegistration := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_status_line", i))
		line.VoIPRegistration = strings.EqualFold(voipRegistration.InnerText(), "Y")
		voipStatus := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_line_state", i))
		line.VoIPStatus = voipStatus.InnerText()
		smsServerStatus := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_smb_login", i))
		line.SMSServerLogin = !reSMSSrv.Match([]byte(smsServerStatus.InnerText()))
		acdStr := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_acd", i))
		acd, err := strconv.Atoi(acdStr.InnerText())
		if err != nil {
			log.Warn().Err(err).Msg("convert AverageCallDuration from string to int failed")
			acd = 0
		}
		acdDur, err := time.ParseDuration(fmt.Sprintf("%ds", acd))
		if err != nil {
			log.Warn().Err(err).Msg("convert AverageCallDuration from int to duration failed")
			acdDur = 0
		}
		line.ACD = acdDur
		asrStr := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_asr", i))
		asr, err := strconv.Atoi(asrStr.InnerText())
		if err != nil {
			log.Warn().Err(err).Msg("convert AverageSuccessRatio from string to int failed")
			asr = 0
		}
		line.ASR = asr
		callDurationStr := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_callt", i))
		callDuration, err := time.ParseDuration(callDurationStr.InnerText())
		if err != nil {
			log.Warn().Err(err).Msg("convert TotalCallDuration from string to duration failed")
			callDuration = 0
		}
		line.TotalCallDuration = callDuration
		ctcount := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_callc", i))
		callscount := strings.Split(ctcount.InnerText(), "/")
		connected := 0
		total := 0
		if len(callscount) == 2 {
			connectedStr := callscount[0]
			totalStr := callscount[1]
			connected, err = strconv.Atoi(connectedStr)
			if err != nil {
				log.Warn().Err(err).Msg("convert ConnectedCalls from string to int failed")
				connected = 0
			}
			total, err = strconv.Atoi(totalStr)
			if err != nil {
				log.Warn().Err(err).Msg("convertTotalCalls from string to int failed")
				total = 0
			}
		}
		line.ConnectedCallsCount = connected
		line.TotalCallsCount = total
		CDRST := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_rct", i))
		cdr, err := time.Parse("2006-01-02 15:04:05", CDRST.InnerText())
		if err != nil {
			log.Warn().Err(err).Msg("convert CDRStartTime from string to Time failed")
			cdr = time.Now()
		}
		line.CDRStartTime = cdr
		rssiStr := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_gsm_signal", i))
		rssi, err := strconv.Atoi(rssiStr.InnerText())
		if err != nil {
			log.Warn().Err(err).Msg("convert RSSI from string to int failed")
			rssi = 0
		}
		line.RSSI = rssi
		carrier := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_gsm_cur_oper", i))
		line.Carrier = carrier.InnerText()
		timeRemainig := CallRemainDuration{}
		timeRemain := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_sim_remain", i))
		if timeRemain.InnerText() == "NO LIMIT" {
			timeRemainig.State = false
			timeRemainig.TimeRemain = 999 * time.Hour
		} else {
			timeRemainig.State = true
			count, err := strconv.Atoi(timeRemain.InnerText())
			if err != nil {
				log.Warn().Err(err).Msg("convert CallsTimeRemaining from string to int failed")
				timeRemainig.TimeRemain = 0
			}
			timeRemainig.TimeRemain, err = time.ParseDuration(fmt.Sprintf("%dm", count))
			if err != nil {
				log.Warn().Err(err).Msg("convert CallsTimeRemaining from int to duration failed")
				timeRemainig.TimeRemain = 0
			}
		}
		line.CallRemainingDuration = timeRemainig
		callRemainig := CallRemainCount{}
		callReamin := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_call_count", i))
		if callReamin.InnerText() == "NO LIMIT" {
			callRemainig.State = false
			callRemainig.CountRemain = 99999999
		} else {
			callRemainig.State = true
			count, err := strconv.Atoi(callReamin.InnerText())
			if err != nil {
				log.Warn().Err(err).Msg("convert CallsCountRemaining from string to int failed")
				callRemainig.CountRemain = 0
			}
			callRemainig.CountRemain = count
		}
		line.CallRemainingCount = callRemainig
		SMSRemaining := SMSRemain{}
		SMSRemain := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_sms_count", i))
		if SMSRemain.InnerText() == "NO LIMIT" {
			SMSRemaining.State = false
			SMSRemaining.CountRemain = 99999999
		} else {
			SMSRemaining.State = true
			count, err := strconv.Atoi(SMSRemain.InnerText())
			if err != nil {
				log.Warn().Err(err).Msg("convert SMSCountRemaining from string to int failed")
				SMSRemaining.CountRemain = 0
			}
			SMSRemaining.CountRemain = count
		}
		line.SMSRemaining = SMSRemaining
		idleTime := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//l%d_nocall_t", i))
		idle, err := strconv.Atoi(idleTime.InnerText())
		if err != nil {
			log.Warn().Err(err).Msg("convert LineIdleTime from string to int failed")
			idle = 0
		}
		idleDuration, err := time.ParseDuration(fmt.Sprintf("%dm", idle))
		if err != nil {
			log.Warn().Err(err).Msg("convert LineIdleTime from int to duration failed")
			idleDuration = 0
		}
		line.IdleTime = idleDuration
		status = append(status, line)
	}
	return status, nil
}
