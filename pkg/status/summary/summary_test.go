package summary

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestReadSummary(t *testing.T) {
	type args struct {
		lines int
		body  string
	}
	tests := []struct {
		name string
		args args
		want []Status
	}{
		{
			name: "parse 8 lines",
			args: args{
				lines: 8,
				body: `<?xml version="1.0" encoding="gb2312"?>
<status>











		<l1_module_status_gsm>1</l1_module_status_gsm>

		<l1_module_title_gsm></l1_module_title_gsm>

		<l1_module_status>&lt;a href="?type=list&amp;down=1&amp;line=1">Y&lt;/a></l1_module_status>
		<l1_module_title>Click to shut down module</l1_module_title>


		<l1_gsm_sim>Y</l1_gsm_sim>




		<l1_gsm_status>Y</l1_gsm_status>



		<l1_status_line>Y</l1_status_line>

		<l1_line_state>IDLE</l1_line_state>

		<l1_sms_login></l1_sms_login>

		<l1_smb_login>&lt;font color="#FF0000">N&lt;/font></l1_smb_login>

		<l1_gsm_signal>26</l1_gsm_signal>

		<l1_gsm_cur_oper>OPERATOR1</l1_gsm_cur_oper>

		<l1_gsm_cur_bst></l1_gsm_cur_bst>

<l1_volte></l1_volte>

		<l1_lac></l1_lac>

		<l1_sim_remain>200</l1_sim_remain>
		<l1_nocall_t>1155</l1_nocall_t>
		<l1_acd>21</l1_acd>
		<l1_asr>100</l1_asr>
		<l1_callt>2m</l1_callt>
		<l1_callc>2/2</l1_callc>
		<l1_rct>2022-04-16 01:28:30</l1_rct>
		<l1_sms_count>100</l1_sms_count>
		<l1_call_count>100</l1_call_count>




		<l2_module_status_gsm>2</l2_module_status_gsm>

		<l2_module_title_gsm></l2_module_title_gsm>

		<l2_module_status>&lt;a href="?type=list&amp;down=1&amp;line=2">Y&lt;/a></l2_module_status>
		<l2_module_title>Click to shut down module</l2_module_title>


		<l2_gsm_sim>Y</l2_gsm_sim>



<l2_gsm_status>Y</l2_gsm_status>


		
		<l2_status_line>Y</l2_status_line>
		
		<l2_line_state>IDLE</l2_line_state>

		<l2_sms_login></l2_sms_login>

		<l2_smb_login>&lt;font color="#FF0000">N&lt;/font></l2_smb_login>

		<l2_gsm_signal>20</l2_gsm_signal>

		<l2_gsm_cur_oper>OPERATOR2</l2_gsm_cur_oper>

		<l2_gsm_cur_bst></l2_gsm_cur_bst>

<l2_volte></l2_volte>

		<l2_lac></l2_lac>

		<l2_sim_remain>NO LIMIT</l2_sim_remain>
		<l2_nocall_t>1025</l2_nocall_t>
		<l2_acd>0</l2_acd>
		<l2_asr>0</l2_asr>
		<l2_callt>0</l2_callt>
		<l2_callc>0/1</l2_callc>
		<l2_rct>2022-10-29 04:01:02</l2_rct>
		<l2_sms_count>NO LIMIT</l2_sms_count>
		<l2_call_count>NO LIMIT</l2_call_count>




		<l3_module_status_gsm>3</l3_module_status_gsm>

		<l3_module_title_gsm></l3_module_title_gsm>

		<l3_module_status>&lt;a href="?type=list&amp;down=0&amp;line=3">&lt;font color="#FF0000">N&lt;/font>&lt;/a></l3_module_status>
		<l3_module_title>Click to turn on module</l3_module_title>


		<l3_gsm_sim>&lt;font color="#FF0000">&lt;/font></l3_gsm_sim>


<l3_gsm_status></l3_gsm_status>

		
		<l3_status_line>&lt;font color="#FF0000">N&lt;/font></l3_status_line>
		
		<l3_line_state>IDLE</l3_line_state>

		<l3_sms_login></l3_sms_login>

		<l3_smb_login>&lt;font color="#FF0000">N&lt;/font></l3_smb_login>

		<l3_gsm_signal></l3_gsm_signal>

		<l3_gsm_cur_oper></l3_gsm_cur_oper>

		<l3_gsm_cur_bst></l3_gsm_cur_bst>

<l3_volte></l3_volte>

		<l3_lac></l3_lac>

		<l3_sim_remain>NO LIMIT</l3_sim_remain>
		<l3_nocall_t>1025</l3_nocall_t>
		<l3_acd></l3_acd>
		<l3_asr></l3_asr>
		<l3_callt></l3_callt>
		<l3_callc>0/0</l3_callc>
		<l3_rct>2022-10-29 04:01:02</l3_rct>
		<l3_sms_count>NO LIMIT</l3_sms_count>
		<l3_call_count>NO LIMIT</l3_call_count>




		<l4_module_status_gsm>4</l4_module_status_gsm>

		<l4_module_title_gsm></l4_module_title_gsm>

		<l4_module_status>&lt;a href="?type=list&amp;down=0&amp;line=4">&lt;font color="#FF0000">N&lt;/font>&lt;/a></l4_module_status>
		<l4_module_title>Click to turn on module</l4_module_title>


		<l4_gsm_sim>&lt;font color="#FF0000">&lt;/font></l4_gsm_sim>


<l4_gsm_status></l4_gsm_status>

		
		<l4_status_line>&lt;font color="#FF0000">N&lt;/font></l4_status_line>
		
		<l4_line_state>IDLE</l4_line_state>

		<l4_sms_login></l4_sms_login>

		<l4_smb_login>&lt;font color="#FF0000">N&lt;/font></l4_smb_login>

		<l4_gsm_signal></l4_gsm_signal>

		<l4_gsm_cur_oper></l4_gsm_cur_oper>

		<l4_gsm_cur_bst></l4_gsm_cur_bst>

<l4_volte></l4_volte>

		<l4_lac></l4_lac>

		<l4_sim_remain>NO LIMIT</l4_sim_remain>
		<l4_nocall_t>1025</l4_nocall_t>
		<l4_acd></l4_acd>
		<l4_asr></l4_asr>
		<l4_callt></l4_callt>
		<l4_callc>0/0</l4_callc>
		<l4_rct>2022-10-29 04:01:02</l4_rct>
		<l4_sms_count>NO LIMIT</l4_sms_count>
		<l4_call_count>NO LIMIT</l4_call_count>




		<l5_module_status_gsm>5</l5_module_status_gsm>

		<l5_module_title_gsm></l5_module_title_gsm>

		<l5_module_status>&lt;a href="?type=list&amp;down=0&amp;line=5">&lt;font color="#FF0000">N&lt;/font>&lt;/a></l5_module_status>
		<l5_module_title>Click to turn on module</l5_module_title>


		<l5_gsm_sim>&lt;font color="#FF0000">&lt;/font></l5_gsm_sim>


<l5_gsm_status></l5_gsm_status>

		
		<l5_status_line>&lt;font color="#FF0000">N&lt;/font></l5_status_line>
		
		<l5_line_state>IDLE</l5_line_state>

		<l5_sms_login></l5_sms_login>

		<l5_smb_login>&lt;font color="#FF0000">N&lt;/font></l5_smb_login>

		<l5_gsm_signal></l5_gsm_signal>

		<l5_gsm_cur_oper></l5_gsm_cur_oper>

		<l5_gsm_cur_bst></l5_gsm_cur_bst>

<l5_volte></l5_volte>

		<l5_lac></l5_lac>

		<l5_sim_remain>NO LIMIT</l5_sim_remain>
		<l5_nocall_t>1025</l5_nocall_t>
		<l5_acd></l5_acd>
		<l5_asr></l5_asr>
		<l5_callt></l5_callt>
		<l5_callc>0/0</l5_callc>
		<l5_rct>2022-10-29 04:01:02</l5_rct>
		<l5_sms_count>NO LIMIT</l5_sms_count>
		<l5_call_count>NO LIMIT</l5_call_count>




		<l6_module_status_gsm>6</l6_module_status_gsm>

		<l6_module_title_gsm></l6_module_title_gsm>

		<l6_module_status>&lt;a href="?type=list&amp;down=0&amp;line=6">&lt;font color="#FF0000">N&lt;/font>&lt;/a></l6_module_status>
		<l6_module_title>Click to turn on module</l6_module_title>


		<l6_gsm_sim>&lt;font color="#FF0000">&lt;/font></l6_gsm_sim>


<l6_gsm_status></l6_gsm_status>

		
		<l6_status_line>&lt;font color="#FF0000">N&lt;/font></l6_status_line>
		
		<l6_line_state>IDLE</l6_line_state>

		<l6_sms_login></l6_sms_login>

		<l6_smb_login>&lt;font color="#FF0000">N&lt;/font></l6_smb_login>

		<l6_gsm_signal></l6_gsm_signal>

		<l6_gsm_cur_oper></l6_gsm_cur_oper>

		<l6_gsm_cur_bst></l6_gsm_cur_bst>

<l6_volte></l6_volte>

		<l6_lac></l6_lac>

		<l6_sim_remain>NO LIMIT</l6_sim_remain>
		<l6_nocall_t>1025</l6_nocall_t>
		<l6_acd></l6_acd>
		<l6_asr></l6_asr>
		<l6_callt></l6_callt>
		<l6_callc>0/0</l6_callc>
		<l6_rct>2022-10-29 04:01:02</l6_rct>
		<l6_sms_count>NO LIMIT</l6_sms_count>
		<l6_call_count>NO LIMIT</l6_call_count>




		<l7_module_status_gsm>7</l7_module_status_gsm>

		<l7_module_title_gsm></l7_module_title_gsm>

		<l7_module_status>&lt;a href="?type=list&amp;down=0&amp;line=7">&lt;font color="#FF0000">N&lt;/font>&lt;/a></l7_module_status>
		<l7_module_title>Click to turn on module</l7_module_title>


		<l7_gsm_sim>&lt;font color="#FF0000">&lt;/font></l7_gsm_sim>


<l7_gsm_status></l7_gsm_status>

		
		<l7_status_line>&lt;font color="#FF0000">N&lt;/font></l7_status_line>
		
		<l7_line_state>IDLE</l7_line_state>

		<l7_sms_login></l7_sms_login>

		<l7_smb_login>&lt;font color="#FF0000">N&lt;/font></l7_smb_login>

		<l7_gsm_signal></l7_gsm_signal>

		<l7_gsm_cur_oper></l7_gsm_cur_oper>

		<l7_gsm_cur_bst></l7_gsm_cur_bst>

<l7_volte></l7_volte>

		<l7_lac></l7_lac>

		<l7_sim_remain>NO LIMIT</l7_sim_remain>
		<l7_nocall_t>1025</l7_nocall_t>
		<l7_acd></l7_acd>
		<l7_asr></l7_asr>
		<l7_callt></l7_callt>
		<l7_callc>0/0</l7_callc>
		<l7_rct>2022-10-29 04:01:02</l7_rct>
		<l7_sms_count>NO LIMIT</l7_sms_count>
		<l7_call_count>NO LIMIT</l7_call_count>




		<l8_module_status_gsm>8</l8_module_status_gsm>

		<l8_module_title_gsm></l8_module_title_gsm>

		<l8_module_status>&lt;a href="?type=list&amp;down=0&amp;line=8">&lt;font color="#FF0000">N&lt;/font>&lt;/a></l8_module_status>
		<l8_module_title>Click to turn on module</l8_module_title>


		<l8_gsm_sim>&lt;font color="#FF0000">&lt;/font></l8_gsm_sim>


<l8_gsm_status></l8_gsm_status>

		
		<l8_status_line>&lt;font color="#FF0000">N&lt;/font></l8_status_line>
		
		<l8_line_state>IDLE</l8_line_state>

		<l8_sms_login></l8_sms_login>

		<l8_smb_login>&lt;font color="#FF0000">N&lt;/font></l8_smb_login>

		<l8_gsm_signal></l8_gsm_signal>

		<l8_gsm_cur_oper></l8_gsm_cur_oper>

		<l8_gsm_cur_bst></l8_gsm_cur_bst>

<l8_volte></l8_volte>

		<l8_lac></l8_lac>

		<l8_sim_remain>NO LIMIT</l8_sim_remain>
		<l8_nocall_t>1025</l8_nocall_t>
		<l8_acd></l8_acd>
		<l8_asr></l8_asr>
		<l8_callt></l8_callt>
		<l8_callc>0/0</l8_callc>
		<l8_rct>2022-10-29 04:01:02</l8_rct>
		<l8_sms_count>NO LIMIT</l8_sms_count>
		<l8_call_count>NO LIMIT</l8_call_count>





















































</status>
				`,
			},
			want: []Status{
				{
					Line:                1,
					GSMModuleStatus:     true,
					SimCard:             true,
					GSMRegistration:     true,
					VoIPRegistration:    true,
					VoIPStatus:          "IDLE",
					SMSServerLogin:      false,
					ACD:                 time.Duration(21 * time.Second),
					ASR:                 100,
					TotalCallDuration:   time.Duration(2 * time.Minute),
					ConnectedCallsCount: 2,
					TotalCallsCount:     2,
					CDRStartTime:        time.Time{},
					RSSI:                26,
					Carrier:             "OPERATOR1",
					BCCH:                nil,
					IdleTime:            time.Duration(1155 * time.Minute),
					CallRemainingCount: CallRemainCount{
						State:       true,
						CountRemain: 100,
					},
					CallRemainingDuration: CallRemainDuration{
						State:      true,
						TimeRemain: time.Duration(200 * time.Minute),
					},
					SMSRemaining: SMSRemain{
						State:       true,
						CountRemain: 100,
					},
				},
				{
					Line:                2,
					GSMModuleStatus:     true,
					SimCard:             true,
					GSMRegistration:     true,
					VoIPRegistration:    true,
					VoIPStatus:          "IDLE",
					SMSServerLogin:      false,
					ACD:                 time.Duration(0 * time.Second),
					ASR:                 0,
					TotalCallDuration:   time.Duration(0 * time.Second),
					ConnectedCallsCount: 0,
					TotalCallsCount:     1,
					CDRStartTime:        time.Time{},
					RSSI:                20,
					Carrier:             "OPERATOR2",
					BCCH:                nil,
					IdleTime:            time.Duration(1025 * time.Minute),
					CallRemainingCount: CallRemainCount{
						State:       false,
						CountRemain: 99999999,
					},
					CallRemainingDuration: CallRemainDuration{
						State:      false,
						TimeRemain: time.Duration(999 * time.Hour),
					},
					SMSRemaining: SMSRemain{
						State:       false,
						CountRemain: 99999999,
					},
				},
				{
					Line:                3,
					GSMModuleStatus:     false,
					SimCard:             false,
					GSMRegistration:     false,
					VoIPRegistration:    false,
					VoIPStatus:          "IDLE",
					SMSServerLogin:      false,
					ACD:                 time.Duration(0 * time.Second),
					ASR:                 0,
					TotalCallDuration:   time.Duration(0 * time.Second),
					ConnectedCallsCount: 0,
					TotalCallsCount:     0,
					CDRStartTime:        time.Time{},
					RSSI:                0,
					Carrier:             "",
					BCCH:                nil,
					IdleTime:            time.Duration(1025 * time.Minute),
					CallRemainingCount: CallRemainCount{
						State:       false,
						CountRemain: 99999999,
					},
					CallRemainingDuration: CallRemainDuration{
						State:      false,
						TimeRemain: time.Duration(999 * time.Hour),
					},
					SMSRemaining: SMSRemain{
						State:       false,
						CountRemain: 99999999,
					},
				},
				{
					Line:                4,
					GSMModuleStatus:     false,
					SimCard:             false,
					GSMRegistration:     false,
					VoIPRegistration:    false,
					VoIPStatus:          "IDLE",
					SMSServerLogin:      false,
					ACD:                 time.Duration(0 * time.Second),
					ASR:                 0,
					TotalCallDuration:   time.Duration(0 * time.Second),
					ConnectedCallsCount: 0,
					TotalCallsCount:     0,
					CDRStartTime:        time.Time{},
					RSSI:                0,
					Carrier:             "",
					BCCH:                nil,
					IdleTime:            time.Duration(1025 * time.Minute),
					CallRemainingCount: CallRemainCount{
						State:       false,
						CountRemain: 99999999,
					},
					CallRemainingDuration: CallRemainDuration{
						State:      false,
						TimeRemain: time.Duration(999 * time.Hour),
					},
					SMSRemaining: SMSRemain{
						State:       false,
						CountRemain: 99999999,
					},
				},
				{
					Line:                5,
					GSMModuleStatus:     false,
					SimCard:             false,
					GSMRegistration:     false,
					VoIPRegistration:    false,
					VoIPStatus:          "IDLE",
					SMSServerLogin:      false,
					ACD:                 time.Duration(0 * time.Second),
					ASR:                 0,
					TotalCallDuration:   time.Duration(0 * time.Second),
					ConnectedCallsCount: 0,
					TotalCallsCount:     0,
					CDRStartTime:        time.Time{},
					RSSI:                0,
					Carrier:             "",
					BCCH:                nil,
					IdleTime:            time.Duration(1025 * time.Minute),
					CallRemainingCount: CallRemainCount{
						State:       false,
						CountRemain: 99999999,
					},
					CallRemainingDuration: CallRemainDuration{
						State:      false,
						TimeRemain: time.Duration(999 * time.Hour),
					},
					SMSRemaining: SMSRemain{
						State:       false,
						CountRemain: 99999999,
					},
				},
				{
					Line:                6,
					GSMModuleStatus:     false,
					SimCard:             false,
					GSMRegistration:     false,
					VoIPRegistration:    false,
					VoIPStatus:          "IDLE",
					SMSServerLogin:      false,
					ACD:                 time.Duration(0 * time.Second),
					ASR:                 0,
					TotalCallDuration:   time.Duration(0 * time.Second),
					ConnectedCallsCount: 0,
					TotalCallsCount:     0,
					CDRStartTime:        time.Time{},
					RSSI:                0,
					Carrier:             "",
					BCCH:                nil,
					IdleTime:            time.Duration(1025 * time.Minute),
					CallRemainingCount: CallRemainCount{
						State:       false,
						CountRemain: 99999999,
					},
					CallRemainingDuration: CallRemainDuration{
						State:      false,
						TimeRemain: time.Duration(999 * time.Hour),
					},
					SMSRemaining: SMSRemain{
						State:       false,
						CountRemain: 99999999,
					},
				},
				{
					Line:                7,
					GSMModuleStatus:     false,
					SimCard:             false,
					GSMRegistration:     false,
					VoIPRegistration:    false,
					VoIPStatus:          "IDLE",
					SMSServerLogin:      false,
					ACD:                 time.Duration(0 * time.Second),
					ASR:                 0,
					TotalCallDuration:   time.Duration(0 * time.Second),
					ConnectedCallsCount: 0,
					TotalCallsCount:     0,
					CDRStartTime:        time.Time{},
					RSSI:                0,
					Carrier:             "",
					BCCH:                nil,
					IdleTime:            time.Duration(1025 * time.Minute),
					CallRemainingCount: CallRemainCount{
						State:       false,
						CountRemain: 99999999,
					},
					CallRemainingDuration: CallRemainDuration{
						State:      false,
						TimeRemain: time.Duration(999 * time.Hour),
					},
					SMSRemaining: SMSRemain{
						State:       false,
						CountRemain: 99999999,
					},
				},
				{
					Line:                8,
					GSMModuleStatus:     false,
					SimCard:             false,
					GSMRegistration:     false,
					VoIPRegistration:    false,
					VoIPStatus:          "IDLE",
					SMSServerLogin:      false,
					ACD:                 time.Duration(0 * time.Second),
					ASR:                 0,
					TotalCallDuration:   time.Duration(0 * time.Second),
					ConnectedCallsCount: 0,
					TotalCallsCount:     0,
					CDRStartTime:        time.Time{},
					RSSI:                0,
					Carrier:             "",
					BCCH:                nil,
					IdleTime:            time.Duration(1025 * time.Minute),
					CallRemainingCount: CallRemainCount{
						State:       false,
						CountRemain: 99999999,
					},
					CallRemainingDuration: CallRemainDuration{
						State:      false,
						TimeRemain: time.Duration(999 * time.Hour),
					},
					SMSRemaining: SMSRemain{
						State:       false,
						CountRemain: 99999999,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadSummary(tt.args.lines, tt.args.body)
			require.NoError(t, err)
			for i, line := range got {
				require.Equal(t, tt.want[i].Line, line.Line)
				require.Equal(t, tt.want[i].GSMModuleStatus, line.GSMModuleStatus)
				require.Equal(t, tt.want[i].SimCard, line.SimCard)
				require.Equal(t, tt.want[i].GSMRegistration, line.GSMRegistration)
				require.Equal(t, tt.want[i].VoIPRegistration, line.VoIPRegistration)
				require.Equal(t, tt.want[i].VoIPStatus, line.VoIPStatus)
				require.Equal(t, tt.want[i].SMSServerLogin, line.SMSServerLogin)
				require.Equal(t, tt.want[i].ACD, line.ACD)
				require.Equal(t, tt.want[i].ASR, line.ASR)
				require.Equal(t, tt.want[i].TotalCallDuration, line.TotalCallDuration)
				require.Equal(t, tt.want[i].ConnectedCallsCount, line.ConnectedCallsCount)
				require.Equal(t, tt.want[i].TotalCallsCount, line.TotalCallsCount)
				require.Equal(t, tt.want[i].RSSI, line.RSSI)
				require.Equal(t, tt.want[i].Carrier, line.Carrier)
				require.Equal(t, tt.want[i].IdleTime, line.IdleTime)
				require.Equal(t, tt.want[i].CallRemainingCount, line.CallRemainingCount)
				require.Equal(t, tt.want[i].CallRemainingDuration, line.CallRemainingDuration)
				require.Equal(t, tt.want[i].SMSRemaining, line.SMSRemaining)
				fmt.Printf("%+v\n", line)
			}
		})
	}
}
