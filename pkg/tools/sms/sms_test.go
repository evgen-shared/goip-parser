package sms

import (
	"errors"
	"sync"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
)

func TestSMSStore_ParseSMS(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	tests := []struct {
		name string
		body string
		want error
	}{
		{
			name: "parse sms inbox page",
			body: `
<html>

<head>
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>GoIP8</title>

<link rel="stylesheet" type="text/css" href="/style/default.css?4">
<style type="text/css">
.visible{
display:block;
}
</style>
<script lang="javascript" src="/script/dynamic.js?2"></script>
<script lang="javascript">
var uptime_s="24982";
if (document.getElementById){

	document.write('<style type="text/css">\n');
	document.write('.invisible{display:none;}\n');
	document.write('</style>\n');
}

function set_uptime(second)
{
	document.getElementById('uptime').innerHTML=[parseInt(second / 60 / 60), parseInt(second / 60 % 60), second % 60].join(":").replace(/\b(\d)\b/g, "0$1");
}

function set_current_time()
{
	var d=new Date(Date.parse(document.getElementById('time_text').innerHTML.slice(-19).replace(/-/g, "/")));
	d.setSeconds(d.getSeconds()+1);

	s = (d.getFullYear())+"-"+((d.getMonth()+1)<10?"0"+(d.getMonth()+1):(d.getMonth()+1))+"-"+(d.getDate()<10?"0"+d.getDate():d.getDate()) +" "
	s += (d.getHours()<10?"0"+(d.getHours()):d.getHours()) + ":"
	s += (d.getMinutes()<10?"0"+(d.getMinutes()):d.getMinutes()) + ":"
	s += (d.getSeconds()<10?"0"+(d.getSeconds()):d.getSeconds())
	document.getElementById('time_text').innerHTML = s;

	if(uptime_s!="") set_uptime(++uptime_s)

	setTimeout('set_current_time()', 1000)
}
setTimeout('set_current_time()', 1000)
</script>
</head>

<body>
<table id="mainframe" height="100%" cellSpacing="0" cellPadding="0" width="768" border="0">
<tr>
<td width="314" class="banner" height="124">
<img height="134" src="/images/title.jpg" width="312" border="0">
		</td>
		<td class="banner" height="124" valign="bottom" width="454">
			<table cellSpacing="0" cellPadding="0" width="100%" height="100%" border="0">
				<tr>
					<td height="24" width="324">

					</td>
					<td width="95" align="right">
						<a href="../zh_CN/status.html"><image height="20" width="60" border="0" src="/images/chs.gif"></a>&nbsp;&nbsp;<br><a href="logout.html" width="60"><span class="time_text">Logout</span></a>&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td height="100" valign="bottom">

												<span class="subtitle">GoIP8</span>

					</td>
										<td valign="bottom" align="right" class="time_text">
												<table cellspacing="0" cellpadding="0" border="0">
													<tr>
													<td class="time_text" >SN(Serial Number):</td>
													<td class="time_text">8MCDRM16112021</td>
													</tr>
													<tr>
													<td class="time_text" >Firmware Version:&nbsp;&nbsp;</td>
													<td class="time_text">GST1610-1.01-70
</td>
													</tr>
													<tr>
													<td class="time_text">Module Version:</td>
													<td class="time_text">M35FAR02A01_RSIM</td>
													</tr>
													<tr>
													<td class="time_text">Uptime:</td>
													<td class="time_text" id="uptime"></td>
													</tr>
													<tr>
													<td class="time_text">Last Login Time:</td>
													<td class="time_text">2022-10-29 21:30:01</td>
													</tr>
													<tr>
													<td class="time_text">Current Time:</td>
													<td class="time_text" id="time_text">2022-10-30 10:56:51</td>
													</tr>
												</table>
										</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colSpan="2">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#ffffff" border="0">
				<tr>
					<td vAlign="top" width="196" class="sidebar">
<script lang="javascript">
function confirm_reset()
{
	return window.confirm("Are you sure to reset to factory default?");
}
function confirm_reboot()
{
	return window.confirm("Are you sure to reboot the device?");
}
</script>
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
		<td colspan="2" align="left" height="15"></td>
	</tr>
	<tr>
		<td width="20"></td>
		<td align="left" height="40">
			<a href="status.html"><div class="title5">Status</div></a></td>
	</tr>
	<tr>
		<td width="20"></td>
		<td align="left" height="40"><a href="config.html"><div class="title5">Configurations</div></a></td>
	</tr>
	<tr>
		<td width="20"></td>
		<td height="40"><div class="title4">Tools</div></td>
	</tr>
	<tr>
		<td width="20"></td>
		<td align="right" valign="top">
			<table border="0" width="82%" cellspacing="0" cellpadding="0">

				<tr>
					<td height="30"><a href="tools.html?type=upgrade"><div class="title3" id="upgrade">Online Upgrade</div></a></td>
				</tr>

				<tr>
					<td height="30"><a href="tools.html?type=password"><div class="title3" id="password">User Management</div></a></td>
				</tr>

				<tr>
					<td height="30"><a href="tools.html?type=ussd"><div class="title3" id="ussd">Send USSD</div></a></td>
				</tr>

				<tr>
					<td height="30"><a href="tools.html?type=sms"><div class="title3" id="sms">Send SMS</div></a></td>
				</tr>
				<tr>
					<td height="30"><a href="tools.html?type=sms_inbox"><div class="title3" id="sms_inbox">SMS InBox</div></a></td>
				</tr>
				<tr>
					<td height="30"><a href="tools.html?type=sms_outbox"><div class="title3" id="sms_outbox">SMS OutBox</div></a></td>
				</tr>

				<tr>
					<td height="30"><a href="tools.html?type=channel"><div class="title3" id="channel">Channel Control</div></a></td>
				</tr>
								<tr>
										<td height="30"><a href="tools.html?type=ping"><div class="title3" id="ping">Ping Test</div></a></td>
								</tr>
								<tr>
										<td height="30"><a href="tools.html?type=dial"><div class="title3" id="dial">Dial Test</div></a></td>
								</tr>
				<tr>
										<td height="30"><a href="tools.html?type=call_logs"><div class="title3" id="call_logs">Call Records</div></a></td>
								</tr>
								<tr>
										<td height="30"><a href="tools.html?type=human"><div class="title3" id="human">Human Behavior</div></a></td>
								</tr>
				<tr>
					<td height="30"><a href="tools.html?type=num"><div id="num" class="title3" id="num">Get Number</div></a></td>
				</tr>

				<tr>
					<td height="30"><a href="tools.html?type=config"><div class="title3" id="config">Backup/Restore</div></a></td>
				</tr>

				<tr>
					<td height="10"></td>
				</tr>

				<tr>
					<form action="reset_config.html" method="post" onsubmit="return confirm_reset()">
						<td height="30"><div class="title3"><input type="submit" value="Reset Config" class="savebutton"></div></td>
					</form>
				</tr>

				<tr>
					<form action="reboot.html" method="post" onsubmit="return confirm_reboot()">
						<td height="30"><div class="title3"><input type="submit" value="Reboot" class="savebutton"></div></td>
					</form>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script lang="javascript">

		if(document.getElementById('sms_inbox')) document.getElementById('sms_inbox').className="title4";

</script>

					</td>
					<td width="828" vAlign="top" class="content">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>

			<div id="tools_page_5_div" style="padding:5px">

<script lang="javascript" src="/script/ajaxroutine.js"></script>
<style type="text/css">
	.table_fix {
		table-layout: fixed;

		/*width: 100%;*/
	}
	.sms_content{
		/*color: #666666;*/
		font-family: helvetica,arial;
		text-indent: 2em;
		padding-bottom: 20px;
	}
	.button1 {
		width: 80px;
		height: 25px;
		font-size: small;
		/*font-weight: bold;*/
		/*letter-spacing: 4px;*//*中文使用*/
		background-color: inherit;
		border-radius: 7.5px;
		color: #003366;
		cursor: pointer;
	}
	.button1:hover{
		/*text-decoration: underline;*/
		color: blue;
	}
	.button2 {
		font-size: inherit;
		/*font-weight: bold;*/
		/*letter-spacing: 4px;*/
		background-color: inherit;
		border-radius: 7.5px;
		color: #003366;
		cursor: pointer;
	}
	.button2:hover {
		color: blue;
	}

	.row11 {
		/*width: 25%;*/
		white-space: nowrap;
		text-align: left;
		vertical-align: bottom;
		border-bottom:1px solid grey;
		height: 40px;
		/*font-weight: bold;*/
		color: #003366;
		font-size: inherit;
	}
	.row12 {
		text-align: right;
		vertical-align: bottom;
		border-bottom:1px solid grey;
		height: 40px;
		color: #003366;
		font-size: inherit;
	}
</style>
<div>
	<div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td colspan="3" class="title2" height="25">SMS InBox</td>
	</tr>

	<tr>
		<td colspan="3" class="text">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
		<td class="text"><input type=radio name="line" value="1" onclick="toggle2('sms_store_tab', 8, 0)" checked>Line 1</td>

				<td class="text"><input type=radio name="line" value="2" onclick="toggle2('sms_store_tab', 8, 1)">Line 2</td>

				<td class="text"><input type=radio name="line" value="3" onclick="toggle2('sms_store_tab', 8, 2)">Line 3</td>

				<td class="text"><input type=radio name="line" value="4" onclick="toggle2('sms_store_tab', 8, 3)">Line 4</td>

				<td class="text"><input type=radio name="line" value="5" onclick="toggle2('sms_store_tab', 8, 4)">Line 5</td>

				<td class="text"><input type=radio name="line" value="6" onclick="toggle2('sms_store_tab', 8, 5)">Line 6</td>

				<td class="text"><input type=radio name="line" value="7" onclick="toggle2('sms_store_tab', 8, 6)">Line 7</td>

				<td class="text"><input type=radio name="line" value="8" onclick="toggle2('sms_store_tab', 8, 7)">Line 8</td>

				</tr>
				<tr>

				</tr>
				<tr>

				</tr>
				<tr>

				</tr>
				</table>
		</td>

	</tr>

	</table>
	</div>

		<div id="sms_store_tab_0_div" class="visable">
				<table border="0" class="table_fix" name="l1_sms_store" id="l1_sms_store" cellpadding="0" cellspacing="0" width="100%">

				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100" align="left" >
						<tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line1" class="button2" value="Delete all messages" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=1&pos=-1'"></td></tr><tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line1" class="button2" value="Clear all messages for all Lines" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=-1&pos=-1'"></td>

						</tr>

				</table>
		</div>

		<div id="sms_store_tab_1_div" class="invisible">
				<table border="0" class="table_fix" name="l2_sms_store" id="l2_sms_store" cellpadding="0" cellspacing="0" width="100%">

				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100" align="left" >
						<tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line2" class="button2" value="Delete all messages" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=2&pos=-1'"></td></tr><tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line2" class="button2" value="Clear all messages for all Lines" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=-1&pos=-1'"></td>

						</tr>

				</table>
		</div>

		<div id="sms_store_tab_2_div" class="invisible">
				<table border="0" class="table_fix" name="l3_sms_store" id="l3_sms_store" cellpadding="0" cellspacing="0" width="100%">

				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100" align="left" >
						<tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line3" class="button2" value="Delete all messages" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=3&pos=-1'"></td></tr><tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line3" class="button2" value="Clear all messages for all Lines" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=-1&pos=-1'"></td>

						</tr>

				</table>
		</div>

		<div id="sms_store_tab_3_div" class="invisible">
				<table border="0" class="table_fix" name="l4_sms_store" id="l4_sms_store" cellpadding="0" cellspacing="0" width="100%">

				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100" align="left" >
						<tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line4" class="button2" value="Delete all messages" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=4&pos=-1'"></td></tr><tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line4" class="button2" value="Clear all messages for all Lines" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=-1&pos=-1'"></td>

						</tr>

				</table>
		</div>

		<div id="sms_store_tab_4_div" class="invisible">
				<table border="0" class="table_fix" name="l5_sms_store" id="l5_sms_store" cellpadding="0" cellspacing="0" width="100%">

				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100" align="left" >
						<tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line5" class="button2" value="Delete all messages" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=5&pos=-1'"></td></tr><tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line5" class="button2" value="Clear all messages for all Lines" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=-1&pos=-1'"></td>

						</tr>

				</table>
		</div>

		<div id="sms_store_tab_5_div" class="invisible">
				<table border="0" class="table_fix" name="l6_sms_store" id="l6_sms_store" cellpadding="0" cellspacing="0" width="100%">

				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100" align="left" >
						<tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line6" class="button2" value="Delete all messages" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=6&pos=-1'"></td></tr><tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line6" class="button2" value="Clear all messages for all Lines" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=-1&pos=-1'"></td>

						</tr>

				</table>
		</div>

		<div id="sms_store_tab_6_div" class="invisible">
				<table border="0" class="table_fix" name="l7_sms_store" id="l7_sms_store" cellpadding="0" cellspacing="0" width="100%">

				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100" align="left" >
						<tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line7" class="button2" value="Delete all messages" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=7&pos=-1'"></td></tr><tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line7" class="button2" value="Clear all messages for all Lines" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=-1&pos=-1'"></td>

						</tr>

				</table>
		</div>

		<div id="sms_store_tab_7_div" class="invisible">
				<table border="0" class="table_fix" name="l8_sms_store" id="l8_sms_store" cellpadding="0" cellspacing="0" width="100%">

				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100" align="left" >
						<tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line8" class="button2" value="Delete all messages" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=8&pos=-1'"></td></tr><tr>
								<td width="50" height="25" class="title1" align="left"><input type="button" name="line8" class="button2" value="Clear all messages for all Lines" onclick="if(confirm('Sure to Delete?')) window.location.href='?action=del&type=sms_inbox&line=-1&pos=-1'"></td>

						</tr>

				</table>
		</div>

</div>
<script language="javascript">

function sms_row_insert(sms_store, sms, pos, line)
{
	for(var j=0;j<20;j++){
		var row1=sms_store.insertRow(sms_store.rows.length);
		var col;
		var sms_time, sms_num, sms_msg, posb;
		var sms_now = sms[pos];

		posb=sms_now.indexOf(",");
		if(posb == -1 ) {
			if(++pos > 19) pos = 19;
			continue;
		}

		sms_time=sms_now.slice(0, posb++ );
		posn=sms_now.indexOf(",", posb);
		if(posn == -1 ) {
			if(++pos > 19) pos = 19;
			continue;
		}
		sms_num=sms_now.slice(posb, posn++ );
		sms_num=sms_num.replace(/[\r\n]/g, "");

		sms_msg=sms_now.slice(posn);

		col = row1.insertCell(0);
		col.setAttribute('class','row11');
		col.innerHTML = "Time:"+sms_time;
		col = row1.insertCell(1);
		col.setAttribute('class','row11');
		col.innerHTML = "From:"+sms_num;
		col = row1.insertCell(2);
		col.setAttribute('class','row12');
		col.innerHTML = "<input type='button' name='delete_"+line+"_"+pos+"' class='button1' value='Delete' onclick='if(confirm(\"sure to delete?\")) window.location.href=\"?action=del&type=sms_inbox&line="+line+"&pos="+pos+"\"'>";
		row2 = sms_store.insertRow();
		col = row2.insertCell(0);
		col.setAttribute('colspan',3);
		col.setAttribute('class','sms_content');
		col.innerHTML = sms_msg;
		if(++pos > 19) pos = 19;
	}
}

var sms;
pos=0;

sms= ["10-26 19:17:13,XXX1,SMS CONTENT 1","10-25 14:48:17,XXX2,SMS CONTENT 2, SMS CONTENT ADDITIONAL","10-21 10:38:40,XXX3,SMS CONTENT 3","10-21 10:38:33,XXX4,SMS CONTENT 4","10-21 10:38:28,XXX5,SMS CONTENT 5","10-19 12:27:15,XXX6,SMS CONTENT 6","10-15 07:38:54,XXX7,SMS CONTENT 7","10-15 07:37:02,XXX8,SMS CONTENT 8","10-05 20:19:36,XXX9,SMS CONTENT 9","09-30 10:02:18,XX10,SMS CONTENT 10","09-29 12:14:49,XX11,SMS CONTENT 11","09-23 13:25:34,XX12,SMS CONTENT 12","09-22 09:19:06,XX13,SMS CONTENT 13","09-13 00:02:26,XX14,SMS CONTENT 14","09-08 18:51:25,XX15,SMS CONTENT 15","09-08 18:50:57,XX16,SMS CONTENT 16","09-08 18:46:18,XX17,SMS CONTENT 17","09-07 12:04:11,XX18,SMS CONTENT 18","09-07 10:06:22,XX19,SMS CONTENT 20","08-25 10:07:52,XX10,SMS CONTENT 20"];
sms_row_insert(l1_sms_store, sms, pos, 1);

sms= ["10-26 19:17:13,YYY1,SMS CONTENT 1","10-25 14:48:17,YYY2,SMS CONTENT 2, SMS CONTENT ADDITIONAL","10-21 10:38:40,YYY3,SMS CONTENT 3","10-21 10:38:33,YYY4,SMS CONTENT 4","10-21 10:38:28,YYY5,SMS CONTENT 5","10-19 12:27:15,YYY6,SMS CONTENT 6","10-15 07:38:54,YYY7,SMS CONTENT 7","10-15 07:37:02,YYY8,SMS CONTENT 8","10-05 20:19:36,YYY9,SMS CONTENT 9","09-30 10:02:18,YY10,SMS CONTENT 10","09-29 12:14:49,YY11,SMS CONTENT 11","09-23 13:25:34,YY12,SMS CONTENT 12","09-22 09:19:06,YY13,SMS CONTENT 13","09-13 00:02:26,YY14,SMS CONTENT 14","09-08 18:51:25,YY15,SMS CONTENT 15","09-08 18:50:57,YY16,SMS CONTENT 16","09-08 18:46:18,YY17,SMS CONTENT 17","09-07 12:04:11,YY18,SMS CONTENT 18","09-07 10:06:22,YY19,SMS CONTENT 20","08-25 10:07:52,YY10,SMS CONTENT 20"];
sms_row_insert(l2_sms_store, sms, pos, 2);

sms= ["","","","","","","","","","","","","","","","","","","",""];
sms_row_insert(l3_sms_store, sms, pos, 3);

sms= ["","","","","","","","","","","","","","","","","","","",""];
sms_row_insert(l4_sms_store, sms, pos, 4);

sms= ["","","","","","","","","","","","","","","","","","","",""];
sms_row_insert(l5_sms_store, sms, pos, 5);

sms= ["","","","","","","","","","","","","","","","","","","",""];
sms_row_insert(l6_sms_store, sms, pos, 6);

sms= ["","","","","","","","","","","","","","","","","","","",""];
sms_row_insert(l7_sms_store, sms, pos, 7);

sms= ["","","","","","","","","","","","","","","","","","","",""];
sms_row_insert(l8_sms_store, sms, pos, 8);

var line_obj = document.getElementsByName("line");

line_obj[0].checked=true;
toggle2('sms_store_tab', 8, 0);

</script>
			</div>

		</td>
	</tr>
</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</body>

</html>

			`,
			want: nil,
		},
		{
			name: "parse wrong response",
			body: `
			`,
			want: ErrInvalidBody,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			store := NewStore(10, 5)
			err := store.ParseSMS(tt.body)
			require.Equal(t, tt.want, err)
			require.NotEmpty(t, store)
		})
	}
}

func TestSMSStore_getSMSfromline(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	tests := []struct {
		name string
		line string
	}{
		{
			name: "parse gsm channel 1 line",
			line: `10-26 19:17:13,XXX1,SMS CONTENT 1","10-25 14:48:17,XXX2,SMS CONTENT 2, SMS CONTENT ADDITIONAL","10-21 10:38:40,XXX3,SMS CONTENT 3","10-21 10:38:33,XXX4,SMS CONTENT 4","10-21 10:38:28,XXX5,SMS CONTENT 5","10-19 12:27:15,XXX6,SMS CONTENT 6","10-15 07:38:54,XXX7,SMS CONTENT 7","10-15 07:37:02,XXX8,SMS CONTENT 8","10-05 20:19:36,XXX9,SMS CONTENT 9","09-30 10:02:18,XX10,SMS CONTENT 10","09-29 12:14:49,XX11,SMS CONTENT 11","09-23 13:25:34,XX12,SMS CONTENT 12","09-22 09:19:06,XX13,SMS CONTENT 13","09-13 00:02:26,XX14,SMS CONTENT 14","09-08 18:51:25,XX15,SMS CONTENT 15","09-08 18:50:57,XX16,SMS CONTENT 16","09-08 18:46:18,XX17,SMS CONTENT 17","09-07 12:04:11,XX18,SMS CONTENT 18","09-07 10:06:22,XX19,SMS CONTENT 20","08-25 10:07:52,XX10,SMS CONTENT 20`,
		},
		{
			name: "parse gsm channel 2 line",
			line: `","","","","","","","","","","","","","","","","","","","`,
		},
	}
	ch := 1
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			store := NewStore(10, 5)
			wg := new(sync.WaitGroup)
			wg.Add(1)
			err := store.getSMSfromline(wg, ch, tt.line)
			wg.Wait()
			if !errors.Is(err, ErrNoContent) {
				require.NoError(t, err)
			}
			sms, err := store.GetCurrentSMSFromStore(ch)
			if !errors.Is(err, ErrNoContent) {
				require.NoError(t, err)
			}
			for _, msg := range sms {
				require.NotEmpty(t, msg.Text)
				require.NotEmpty(t, msg.Date)
				require.NotEmpty(t, msg.From)
				require.False(t, msg.Read)
			}
		})
	}
}

func TestSMSStore_Stop(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	tests := []struct {
		name string
		line string
	}{
		{
			name: "parse, clean and stop",
			line: `10-26 19:17:13,XXX1,SMS CONTENT 1","10-25 14:48:17,XXX2,SMS CONTENT 2, SMS CONTENT ADDITIONAL","10-21 10:38:40,XXX3,SMS CONTENT 3","10-21 10:38:33,XXX4,SMS CONTENT 4","10-21 10:38:28,XXX5,SMS CONTENT 5","10-19 12:27:15,XXX6,SMS CONTENT 6","10-15 07:38:54,XXX7,SMS CONTENT 7","10-15 07:37:02,XXX8,SMS CONTENT 8","10-05 20:19:36,XXX9,SMS CONTENT 9","09-30 10:02:18,XX10,SMS CONTENT 10","09-29 12:14:49,XX11,SMS CONTENT 11","09-23 13:25:34,XX12,SMS CONTENT 12","09-22 09:19:06,XX13,SMS CONTENT 13","09-13 00:02:26,XX14,SMS CONTENT 14","09-08 18:51:25,XX15,SMS CONTENT 15","09-08 18:50:57,XX16,SMS CONTENT 16","09-08 18:46:18,XX17,SMS CONTENT 17","09-07 12:04:11,XX18,SMS CONTENT 18","09-07 10:06:22,XX19,SMS CONTENT 20","08-25 10:07:52,XX10,SMS CONTENT 20`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			store := NewStore(5, 3)
			wg := new(sync.WaitGroup)
			wg.Add(1)
			err := store.getSMSfromline(wg, 1, tt.line)
			require.NoError(t, err)
			go func() {
				time.Sleep(5 * time.Second)
				store.Stop()
			}()
			<-store.stop
		})
	}
}

func TestSMSStore_GetUnreadSMS(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	store := NewStore(10, 5)
	tests := []struct {
		name string
		line string
	}{
		{
			name: "get unread messages",
			line: `10-26 19:17:13,XXX1,SMS CONTENT 1","10-25 14:48:17,XXX2,SMS CONTENT 2, SMS CONTENT ADDITIONAL","10-21 10:38:40,XXX3,SMS CONTENT 3","10-21 10:38:33,XXX4,SMS CONTENT 4","10-21 10:38:28,XXX5,SMS CONTENT 5","10-19 12:27:15,XXX6,SMS CONTENT 6","10-15 07:38:54,XXX7,SMS CONTENT 7","10-15 07:37:02,XXX8,SMS CONTENT 8","10-05 20:19:36,XXX9,SMS CONTENT 9","09-30 10:02:18,XX10,SMS CONTENT 10","09-29 12:14:49,XX11,SMS CONTENT 11","09-23 13:25:34,XX12,SMS CONTENT 12","09-22 09:19:06,XX13,SMS CONTENT 13","09-13 00:02:26,XX14,SMS CONTENT 14","09-08 18:51:25,XX15,SMS CONTENT 15","09-08 18:50:57,XX16,SMS CONTENT 16","09-08 18:46:18,XX17,SMS CONTENT 17","09-07 12:04:11,XX18,SMS CONTENT 18","09-07 10:06:22,XX19,SMS CONTENT 20","08-25 10:07:52,XX10,SMS CONTENT 20`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			wg := new(sync.WaitGroup)
			wg.Add(1)
			err := store.getSMSfromline(wg, 1, tt.line)
			wg.Wait()
			require.NoError(t, err)
			unread, err := store.GetUnreadSMS(1)
			require.NoError(t, err)
			require.NotEmpty(t, unread)
			require.Equal(t, 20, len(unread))
		})
	}
}

func TestSMSStore_GetUnsentSMS(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	store := NewStore(10, 5)
	type args struct {
		to   string
		text string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "get unsent messages",
			args: args{
				to:   "+1234567890",
				text: `qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sms := NewSMS(tt.args.to, tt.args.text)
			store.addSMS(1, sms)
			unsent, err := store.GetUnsentSMS(1)
			require.NoError(t, err)
			require.NotEmpty(t, unsent)
			require.Equal(t, 1, len(unsent))
		})
	}
}

func TestNewSMS(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	type args struct {
		to   string
		text string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "create new sms",
			args: args{
				to:   "+1234567890",
				text: "this is sample message",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sms := NewSMS(tt.args.to, tt.args.text)
			require.Equal(t, tt.args.to, sms.To)
			require.Equal(t, tt.args.text, sms.Text)
			now := time.Now()
			require.True(t, now.After(sms.Date))
		})
	}
}

func TestSMSStore_findSMS(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	store := NewStore(600, 60)
	wg := new(sync.WaitGroup)
	wg.Add(1)
	go store.getSMSfromline(wg, 1, `10-26 19:17:13,XXX1,SMS CONTENT 1","10-25 14:48:17,XXX2,SMS CONTENT 2, SMS CONTENT ADDITIONAL","10-21 10:38:40,XXX3,SMS CONTENT 3","10-21 10:38:33,XXX4,SMS CONTENT 4","10-21 10:38:28,XXX5,SMS CONTENT 5","10-19 12:27:15,XXX6,SMS CONTENT 6","10-15 07:38:54,XXX7,SMS CONTENT 7","10-15 07:37:02,XXX8,SMS CONTENT 8","10-05 20:19:36,XXX9,SMS CONTENT 9","09-30 10:02:18,XX10,SMS CONTENT 10","09-29 12:14:49,XX11,SMS CONTENT 11","09-23 13:25:34,XX12,SMS CONTENT 12","09-22 09:19:06,XX13,SMS CONTENT 13","09-13 00:02:26,XX14,SMS CONTENT 14","09-08 18:51:25,XX15,SMS CONTENT 15","09-08 18:50:57,XX16,SMS CONTENT 16","09-08 18:46:18,XX17,SMS CONTENT 17","09-07 12:04:11,XX18,SMS CONTENT 18","09-07 10:06:22,XX19,SMS CONTENT 20","08-25 10:07:52,XX10,SMS CONTENT 20`)
	wg.Wait()
	tests := []struct {
		name string
		sms  string
		want bool
	}{
		{
			name: "get existing message",
			sms:  `10-26 19:17:13,XXX1,SMS CONTENT 1`,
			want: true,
		},
		{
			name: "get not existing message",
			sms:  `10-26 19:17:14,XXX1,SMS CONTENT 1`,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sms, err := parseSingleMessage(tt.sms)
			require.NoError(t, err)
			require.Equal(t, tt.want, store.findSMS(1, sms))
		})
	}
}

func Test_parseSingleMessage(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	tests := []struct {
		name string
		sms  string
		want string
	}{
		{
			name: "parse simple",
			sms:  `10-26 19:17:13,XXX1,SMS CONTENT 1`,
			want: `SMS CONTENT 1`,
		},
		{
			name: "parse with comma",
			sms:  `10-26 19:17:14,XXX1,SMS CONTENT 1, with comma`,
			want: `SMS CONTENT 1, with comma`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sms, err := parseSingleMessage(tt.sms)
			require.NoError(t, err)
			require.Equal(t, tt.want, sms.Text)
		})
	}
}
