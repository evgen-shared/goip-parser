package sms

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/antchfx/xmlquery"
	"github.com/rs/zerolog"

	"gitlab.com/dummy-gopher/goip-parser/pkg/internal/logging"
)

var (
	log                  zerolog.Logger                       // package logger
	ErrInvalidSMS        = errors.New("sms content invalid")  // error for invalid single SMSs line of GSM channel
	ErrInvalidBody       = errors.New("invalid http body")    // error if http body content not contains required strings
	ErrNoContent         = errors.New("no sms available")     // error for storage if no messages saved
	ErrTimeInvalid       = errors.New("time string invalid")  // error for parsing sms if time string not parsable
	ErrSMSSentInProgress = errors.New("send sms in progress") // error for waiting sms send state
	ErrSMSSentFailed     = errors.New("send sms failed")      // error if sms send failed
	ErrSMSSSentNotFound  = errors.New("sms key not found")    // error id unique sms key not found
)

func init() {
	log = logging.New().With().Str("package", "tools").Str("subpackage", "sms").Logger()

}

type SMSStore struct {
	mux   sync.RWMutex    // mutex for thread safety
	wg    *sync.WaitGroup // subroutines wait group
	stop  chan struct{}   // cleaner stop channel
	Store map[int][]SMS   // sms inmemory storage
}

// SMS - universal for inbox or outbox struct of single SMS message
type SMS struct {
	Direction string    // messade direction. only in or out
	Date      time.Time // event time
	From      string    // sender id
	To        string    // receiver id
	Text      string    // sms content
	Read      bool      // sms was sent to user
	Sent      bool      // sms was sent to remote
	UID       string    // unique sms ID
}

// NewStore - create new sms storage instance
func NewStore(freeze, interval int) *SMSStore {
	store := new(SMSStore)
	store.wg = new(sync.WaitGroup)
	store.mux = sync.RWMutex{}
	store.stop = make(chan struct{})
	store.Store = make(map[int][]SMS)
	log.Trace().Msg("sms storage initialized")
	go store.cleaner(freeze, interval)
	return store
}

// NewSMS - create new SMS message. May used only with sending SMS
//   to - destination phone number
//   text - text of sms message
func NewSMS(to, text string) SMS {
	msg := SMS{
		Direction: "out",
		Date:      time.Now(),
		To:        to,
		Text:      text,
	}
	log.Trace().Msgf("new sms: %+v", msg)
	return msg
}

// findSMS - check instance for SMS entry
func (sms *SMSStore) findSMS(ch int, s SMS) bool {
	sms.mux.RLock()
	defer sms.mux.RUnlock()
	if v, ok := sms.Store[ch]; ok {
		for _, vv := range v {
			if strings.EqualFold(vv.UID, s.UID) {
				return true
			}
		}
	}
	return false
}

// assSMS - add new SMS entry to sms storage
func (sms *SMSStore) addSMS(ch int, s SMS) *SMSStore {
	sms.mux.Lock()
	defer sms.mux.Unlock()
	if _, ok := sms.Store[ch]; ok {
		log.Debug().Msgf("add sms to existing gsm channel %d sms store", ch)
		log.Trace().Msgf("added content: %+v", s)
		sms.Store[ch] = append(sms.Store[ch], s)
	} else {
		log.Debug().Msgf("create new gsm channel %d sms store", ch)
		sms.Store[ch] = make([]SMS, 0)
		log.Trace().Msgf("added content: %+v", s)
		sms.Store[ch] = append(sms.Store[ch], s)
	}
	return sms
}

// deleteOld - delete SMS entries from storage older than 5 days from time.Now()
func (sms *SMSStore) deleteOld() *SMSStore {
	sms.mux.Lock()
	defer sms.mux.Unlock()
	now := time.Now()
	for ch, stored := range sms.Store {
		newStore := make([]SMS, 0)
		if len(stored) == 0 {
			log.Debug().Msg("no saved sms")
			continue
		}
		for _, v := range stored {
			log.Trace().Msgf("time diff: %v", now.Sub(v.Date))
			if now.Sub(v.Date) > time.Duration(5*24*time.Hour) {
				log.Trace().Msg("skip old sms")
				continue
			}
			log.Trace().Msg("sms not old")
			newStore = append(newStore, v)
		}
		sms.Store[ch] = newStore
	}
	return sms
}

// cleaner - automater gouroutine to delete old enties.\
//   freeze - interval to sleep on first run
//   interval - repeat interval
func (sms *SMSStore) cleaner(freeze, interval int) {
	sms.wg.Add(1)
	defer sms.wg.Done()
	ticker := time.NewTicker(time.Duration(interval) * time.Second)
	log.Info().Msgf("old sms cleaner started with interval %d seconds", interval)
	log.Trace().Msgf("start sleeping. sleep for %d seconds", freeze)
	time.Sleep(time.Duration(freeze) * time.Second)
	log.Trace().Msg("fist sleep ended")
	for {
		select {
		case <-sms.stop:
			log.Info().Msg("cleaner. stop signal received")
			ticker.Stop()
			log.Info().Msg("cleaner process stopped")
			return
		case <-ticker.C:
			sms.deleteOld()
		}
	}
}

// Stop - stops SMS processes such cleaner or getSMSfromline
func (sms *SMSStore) Stop() {
	sms.mux.Lock()
	defer sms.mux.Unlock()
	log.Info().Msg("received stop command")
	close(sms.stop)
	sms.wg.Wait()
}

// SetRead - set sms as read
func (sms *SMSStore) SetRead(ch int, uid string) error {
	store, ok := sms.Store[ch]
	if !ok {
		return ErrNoContent
	}
	for i, msg := range store {
		if strings.EqualFold(msg.UID, uid) {
			sms.mux.Lock()
			sms.Store[ch][i].Read = true
			sms.mux.Unlock()
			break
		}
	}
	return nil
}

// SetSent - set sms as sent
func (sms *SMSStore) SetSent(ch int, uid string) error {
	store, ok := sms.Store[ch]
	if !ok {
		return ErrNoContent
	}
	for i, msg := range store {
		if strings.EqualFold(msg.UID, uid) {
			sms.mux.Lock()
			sms.Store[ch][i].Sent = true
			sms.mux.Unlock()
			break
		}
	}
	return nil
}

func parseSingleMessage(line string) (SMS, error) {
	log.Trace().Msgf("start parsing message %s", line)
	val := strings.Split(line, ",")
	if len(val) < 3 {
		log.Error().Msg("message split len less 3")
		return SMS{}, ErrInvalidSMS
	}
	receiveTime, err := time.Parse("2006-01-02 15:04:05", fmt.Sprintf("%d-%s", time.Now().Year(), val[0]))
	if err != nil {
		log.Warn().Err(err).Msg("parse sms receive time failed")
		return SMS{}, ErrTimeInvalid
	}
	log.Trace().Msgf("Receive time: %v", receiveTime)
	sender := val[1]
	log.Trace().Msgf("Sender: %s", sender)
	text := strings.Join(val[2:], ",")
	log.Trace().Msgf("SMS text: %v", text)
	s := SMS{
		Direction: "in",
		Read:      false,
		Date:      receiveTime,
		From:      sender,
		Text:      text,
	}
	uid := sha1.New()
	uid.Write([]byte(s.Text))
	uid.Write([]byte(s.From))
	uid.Write([]byte(s.To))
	uid.Write([]byte(s.Direction))
	uid.Write([]byte(strconv.FormatInt(s.Date.Unix(), 10)))
	s.UID = fmt.Sprintf("%x", uid.Sum(nil))
	log.Trace().Msgf("parsed message: %+v", s)
	return s, nil
}

// getSMSfromline - parse single line with SMSs from GSM channel
func (sms *SMSStore) getSMSfromline(wg *sync.WaitGroup, ch int, line string) error {
	defer wg.Done()
	log.Trace().Msgf("start parsing line %s for line %d", line, ch)
	sms.wg.Add(1)
	defer sms.wg.Done()
	content := strings.Split(line, "\",\"")
	for _, c := range content {
		if len(c) == 0 {
			log.Debug().Msg("sms content empty")
			continue
		}
		s, err := parseSingleMessage(c)
		if errors.Is(err, ErrInvalidSMS) || errors.Is(err, ErrTimeInvalid) {
			log.Warn().Err(err).Msg("message not recognized")
			continue
		}
		if sms.findSMS(ch, s) {
			continue
		} else {
			sms.addSMS(ch, s)
		}
	}
	return nil
}

// ParseSMS - parse full html body from GoIP gateway
func (sms *SMSStore) ParseSMS(body string) error {
	log.Trace().Msg("start parse sms content")
	start := strings.Index(body, "var sms;")
	stop := strings.Index(body, "var line_obj")
	if start == -1 || stop == -1 {
		return ErrInvalidBody
	}
	smsLine := body[start:stop]
	l := strings.Split(smsLine, ";\n")
	lines := make([]string, 0)
	for _, line := range l {
		log.Trace().Msgf("sms line: %s", line)
		if !strings.Contains(line, "sms") || strings.Contains(line, "var sms") {
			continue
		}
		if strings.Contains(line, "sms=") {
			start := strings.Index(line, "sms=")
			line = line[start:]
			log.Trace().Msgf("found line: %s", line)
		}
		if strings.Contains(line, "sms_row_insert") {
			start := strings.Index(line, "sms_row_insert")
			line = line[start:]
			log.Trace().Msgf("found line: %s", line)
		}
		lines = append(lines, line)
	}
	log.Trace().Msgf("lines: %v", lines)
	smsLines := make(map[int]string)
	for i, line := range lines {
		if strings.Contains(line, "sms_row_insert") {
			tmp := strings.Split(line, ",")
			channel := tmp[len(tmp)-1]
			start := strings.Index(channel, " ")
			stop := strings.Index(channel, ")")
			channel = channel[start+1 : stop]
			ch, err := strconv.Atoi(channel)
			if err != nil {
				log.Warn().Err(err).Msg("convert channel number from string to int failed")
				ch = i / 2
			}
			smsLines[ch] = lines[i-1]
		}
	}
	log.Trace().Msgf("prepared: %+v", smsLines)
	wg := new(sync.WaitGroup)
	for k, v := range smsLines {
		start := strings.Index(v, "[")
		stop := strings.Index(v, "]")
		if start == -1 || stop == -1 {
			return ErrInvalidBody
		}
		line := v[start+2 : stop-1]
		log.Trace().Msg("send sms lines to worker")
		wg.Add(1)
		go sms.getSMSfromline(wg, k, line)
	}
	wg.Wait()
	return nil
}

// GetCurrentSMSFromStore - get current inmemory stored sms messages of GSM channel
//   ch - channel number
func (sms *SMSStore) GetCurrentSMSFromStore(ch int) ([]SMS, error) {
	sms.mux.RLock()
	defer sms.mux.RUnlock()
	if v, ok := sms.Store[ch]; ok {
		return v, nil
	}
	return nil, ErrNoContent
}

// GetUnreadSMS - get unread messages from SMSs storage of GSM channel
//   ch - channel number
func (sms *SMSStore) GetUnreadSMS(ch int) ([]SMS, error) {
	sms.mux.RLock()
	defer sms.mux.RUnlock()
	unread := make([]SMS, 0)
	if v, ok := sms.Store[ch]; ok {
		for _, vv := range v {
			if !vv.Read && vv.Direction == "in" {
				unread = append(unread, vv)
			}
		}
		return unread, nil
	}
	return nil, ErrNoContent
}

// GetUnsentSMS - get unsent messages from SMSs storage of GSM channel
//   ch - channel number
func (sms *SMSStore) GetUnsentSMS(ch int) ([]SMS, error) {
	sms.mux.RLock()
	defer sms.mux.RUnlock()
	unsent := make([]SMS, 0)
	if v, ok := sms.Store[ch]; ok {
		for _, vv := range v {
			if !vv.Sent && vv.Direction == "out" {
				unsent = append(unsent, vv)
			}
		}
		return unsent, nil
	}
	return nil, ErrNoContent
}

// sentStatus - parse sent sms state
//   line - gateway line number
//   body - status xml body
//   smskey - unique short uuid string
func sentStatus(line int, body, smskey string) error {
	xmlResponse, err := xmlquery.Parse(strings.NewReader(body))
	if err != nil {
		log.Error().Err(err).Msg("xml parse failed")
		return err
	}
	key := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//smskey%d", line))
	if key.InnerText() != smskey {
		log.Error().Err(err).Msg("unique sms key not found")
		return ErrSMSSSentNotFound
	}
	status := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//status%d", line))
	if status.InnerText() != "DONE" {
		if status.InnerText() == "STARTED" {
			log.Debug().Err(err).Msg("ussd request in progress")
			return ErrSMSSentInProgress
		}
		log.Debug().Err(err).Msg("ussd request failed")
		return ErrSMSSentFailed
	}
	return nil
}
