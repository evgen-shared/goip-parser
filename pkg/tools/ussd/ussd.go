package ussd

import (
	"errors"
	"fmt"
	"strings"

	"github.com/antchfx/xmlquery"
	"github.com/rs/zerolog"
	"gitlab.com/dummy-gopher/goip-parser/pkg/internal/logging"
)

var (
	log              zerolog.Logger
	ErrUSSDIncomlete = errors.New("ussd answer not ready") // ussd request in progress
	ErrUSSDNotFound  = errors.New("ussd answer not found") // ussd unique key not found
	ErrUSSDFailed    = errors.New("ussd request failed")   // ussd request failed
)

func init() {
	log = logging.New().With().Str("package", "tools").Str("subpackage", "ussd").Logger()
}

// ParseUSSD - parse ussd response body
//   line - gateway line number
//   body - response body (xml)
//   smskey - unique ussd shoet uuid key
func ParseUSSD(line int, body, smskey string) (string, error) {
	xmlResponse, err := xmlquery.Parse(strings.NewReader(body))
	if err != nil {
		log.Error().Err(err).Msg("xml parse failed")
		return "", err
	}
	key := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//smskey%d", line))
	if key.InnerText() != smskey {
		log.Error().Err(err).Msg("unique sms key not found")
		return "", ErrUSSDNotFound
	}
	status := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//status%d", line))
	if status.InnerText() != "DONE" {
		if status.InnerText() == "STARTED" {
			log.Debug().Err(err).Msg("ussd request in progress")
			return "", ErrUSSDIncomlete
		}
		log.Debug().Err(err).Msg("ussd request failed")
		return "", ErrUSSDFailed
	}
	answer := xmlquery.FindOne(xmlResponse, fmt.Sprintf("//error%d", line))
	return answer.InnerText(), nil
}
