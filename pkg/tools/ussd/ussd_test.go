package ussd

import (
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
)

func TestParseUSSD(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	type args struct {
		line int
		body string
		key  string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr error
	}{
		{
			name:    "parse complete",
			want:    "some answer",
			wantErr: nil,
			args: args{
				line: 1,
				key:  "6362b49b",
				body: `
<send-sms-status>
<smskey1>6362b49b</smskey1>
<status1>DONE</status1>
<error1>some answer</error1>
<smskey2/>
<status2>DONE</status2>
<error2/>
<smskey3/>
<status3>DONE</status3>
<error3/>
<smskey4/>
<status4>DONE</status4>
<error4/>
<smskey5/>
<status5>DONE</status5>
<error5/>
<smskey6/>
<status6>DONE</status6>
<error6/>
<smskey7/>
<status7>DONE</status7>
<error7/>
<smskey8/>
<status8>DONE</status8>
<error8/>
<smskey9/>
<status9/>
<error9/>
<smskey10/>
<status10/>
<error10/>
<smskey11/>
<status11/>
<error11/>
<smskey12/>
<status12/>
<error12/>
<smskey13/>
<status13/>
<error13/>
<smskey14/>
<status14/>
<error14/>
<smskey15/>
<status15/>
<error15/>
<smskey16/>
<status16/>
<error16/>
<smskey17/>
<status17/>
<error17/>
<smskey18/>
<status18/>
<error18/>
<smskey19/>
<status19/>
<error19/>
<smskey20/>
<status20/>
<error20/>
<smskey21/>
<status21/>
<error21/>
<smskey22/>
<status22/>
<error22/>
<smskey23/>
<status23/>
<error23/>
<smskey24/>
<status24/>
<error24/>
<smskey25/>
<status25/>
<error25/>
<smskey26/>
<status26/>
<error26/>
<smskey27/>
<status27/>
<error27/>
<smskey28/>
<status28/>
<error28/>
<smskey29/>
<status29/>
<error29/>
<smskey30/>
<status30/>
<error30/>
<smskey31/>
<status31/>
<error31/>
<smskey32/>
<status32/>
<error32/>
</send-sms-status>
				`,
			},
		},
		{
			name:    "parse incomplete",
			want:    "",
			wantErr: ErrUSSDIncomlete,
			args: args{
				line: 1,
				key:  "6362b49b",
				body: `
<send-sms-status>
<smskey1>6362b49b</smskey1>
<status1>STARTED</status1>
<error1>some answer</error1>
<smskey2/>
<status2>DONE</status2>
<error2/>
<smskey3/>
<status3>DONE</status3>
<error3/>
<smskey4/>
<status4>DONE</status4>
<error4/>
<smskey5/>
<status5>DONE</status5>
<error5/>
<smskey6/>
<status6>DONE</status6>
<error6/>
<smskey7/>
<status7>DONE</status7>
<error7/>
<smskey8/>
<status8>DONE</status8>
<error8/>
<smskey9/>
<status9/>
<error9/>
<smskey10/>
<status10/>
<error10/>
<smskey11/>
<status11/>
<error11/>
<smskey12/>
<status12/>
<error12/>
<smskey13/>
<status13/>
<error13/>
<smskey14/>
<status14/>
<error14/>
<smskey15/>
<status15/>
<error15/>
<smskey16/>
<status16/>
<error16/>
<smskey17/>
<status17/>
<error17/>
<smskey18/>
<status18/>
<error18/>
<smskey19/>
<status19/>
<error19/>
<smskey20/>
<status20/>
<error20/>
<smskey21/>
<status21/>
<error21/>
<smskey22/>
<status22/>
<error22/>
<smskey23/>
<status23/>
<error23/>
<smskey24/>
<status24/>
<error24/>
<smskey25/>
<status25/>
<error25/>
<smskey26/>
<status26/>
<error26/>
<smskey27/>
<status27/>
<error27/>
<smskey28/>
<status28/>
<error28/>
<smskey29/>
<status29/>
<error29/>
<smskey30/>
<status30/>
<error30/>
<smskey31/>
<status31/>
<error31/>
<smskey32/>
<status32/>
<error32/>
</send-sms-status>
				`,
			},
		},
		{
			name:    "parse invalid",
			want:    "",
			wantErr: ErrUSSDNotFound,
			args: args{
				line: 1,
				key:  "6362b33b",
				body: `
<send-sms-status>
<smskey1>6362b49b</smskey1>
<status1>DONE</status1>
<error1>some answer</error1>
<smskey2/>
<status2>DONE</status2>
<error2/>
<smskey3/>
<status3>DONE</status3>
<error3/>
<smskey4/>
<status4>DONE</status4>
<error4/>
<smskey5/>
<status5>DONE</status5>
<error5/>
<smskey6/>
<status6>DONE</status6>
<error6/>
<smskey7/>
<status7>DONE</status7>
<error7/>
<smskey8/>
<status8>DONE</status8>
<error8/>
<smskey9/>
<status9/>
<error9/>
<smskey10/>
<status10/>
<error10/>
<smskey11/>
<status11/>
<error11/>
<smskey12/>
<status12/>
<error12/>
<smskey13/>
<status13/>
<error13/>
<smskey14/>
<status14/>
<error14/>
<smskey15/>
<status15/>
<error15/>
<smskey16/>
<status16/>
<error16/>
<smskey17/>
<status17/>
<error17/>
<smskey18/>
<status18/>
<error18/>
<smskey19/>
<status19/>
<error19/>
<smskey20/>
<status20/>
<error20/>
<smskey21/>
<status21/>
<error21/>
<smskey22/>
<status22/>
<error22/>
<smskey23/>
<status23/>
<error23/>
<smskey24/>
<status24/>
<error24/>
<smskey25/>
<status25/>
<error25/>
<smskey26/>
<status26/>
<error26/>
<smskey27/>
<status27/>
<error27/>
<smskey28/>
<status28/>
<error28/>
<smskey29/>
<status29/>
<error29/>
<smskey30/>
<status30/>
<error30/>
<smskey31/>
<status31/>
<error31/>
<smskey32/>
<status32/>
<error32/>
</send-sms-status>
				`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseUSSD(tt.args.line, tt.args.body, tt.args.key)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.want, got)
		})
	}
}
