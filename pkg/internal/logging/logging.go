package logging

import (
	"os"
	"sync"

	"github.com/rs/zerolog"
)

var (
	once   sync.Once
	logger zerolog.Logger
)

func New() zerolog.Logger {
	once.Do(func() {
		logger = zerolog.New(zerolog.SyncWriter(os.Stderr)).With().Timestamp().Caller().Str("library", "goip parser").Logger()
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
		logger.Info().Msg("logger initialized")
	})
	return logger
}
