# Goip Parser

This is library to parse GoIP GSM Gateway http answers.
Tested with GoIP8 firmware GST1610-1.01-70.pkg

## TODO

> [] Minimal test cover 80%   
> [] ALL function have unit test   

### Parse Status page  
> [x] Parse Summary (home)   
> [] Parse General page   
> [] Parse SIM page   
> [] Parse SIM Call Forward page   

### Parse Configuration   
### Parse Tools  
> [] Parse User management   
> [x] Parse SMS Inbox page    
> [x] Parse SMS Outbox page   
> [] Parse Send USSD    
> [] Parse Send SMS     